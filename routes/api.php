<?php

use App\Http\Controllers\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/users', [UserController::class, 'list']);
Route::get('/thumbprints/{thumbprint}', [UserController::class, 'findByThumbprint']);
Route::post('/users', [UserController::class, 'create']);
Route::get('/users/{user_id}/public-key', [UserController::class, 'getPublicKey']);
Route::get('/users/{user_id}', [UserController::class, 'get']);
Route::put('/users/{user_id}', [UserController::class, 'update']);
Route::delete('/users/{user_id}', [UserController::class, 'delete']);
Route::post('/users/verify-signature', [UserController::class, 'verifySignature']);
